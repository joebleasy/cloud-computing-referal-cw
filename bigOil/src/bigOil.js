const express = require("express");
const mongoose = require("mongoose");
const send = require("./send.js");
const receive = require("./receive.js");
const os = require("os");
const PORT = 3000;
const app = express();
const { Etcd3 } = require('etcd3');

// Application config
const mongoUrl = "mongodb://mongo-1:27501,mongo-2:27501,mongo-3:27501";

// Create etcd client
const client = new Etcd3({hosts: ["http://etcd1:2379","http://etcd2:2379","http://etcd3:2379"]});
const election = client.election('big-oil');

// Start a campaign
const campaign = election.campaign(os.hostname())
campaign.on('elected', () => {
  // Any leader specific actions should be started by this function
  console.log("Leader node selected")
});

app.use(express.json());

/**
* The mongoSchema.
*/
const mongoSchema = new mongoose.Schema({
  _id: mongoose.Types.ObjectId,
  rigID: Number,
  deviceId: Number,
  sensorData: String,
  timestamp: Date
});

const model = mongoose.model("model", mongoSchema, "model");

mongoose.connect(mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true });

app.post("/", async (request, response, _) => {
  const insight = await saveData(request.body);
  send(JSON.stringify(request.body));
  response.json(insight);
})

app.listen(PORT, () => {
  console.log(`application listening on port: ` + PORT);
});

async function saveData(data) {
  const newModel = new model({
    _id: new mongoose.Types.ObjectId(),
    rigID: data.rigID,
    deviceId: data.deviceId,
    sensorData: data.sensorData,
    timestamp: data.timestamp
  });
  
  return newModel.save()
  .then(() => {
    return newModel;
  })
  .catch((err) => { return err; });
}
