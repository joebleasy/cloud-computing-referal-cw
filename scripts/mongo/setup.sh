#!/bin/bash
until mongo --host mongo-1 --eval "print(\"Waiting for connection.\")"
do
  sleep 3
done
echo "Connection established. Creating replica group start."
mongo --host mongo-1 <<EOF
  rs.initiate(
    {
      _id: "rg-0",
      members: [
        { _id: 0, host: "mongo-1:27017" },
        { _id: 1, host: "mongo-2:27017" },
        { _id: 2, host: "mongo-3:27017" }
      ]
    }
  )  
EOF
echo "Replica group end."